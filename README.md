# Suricata Emerging Threats Updater

## Prerequisites
Suricata 4

## Installation
wget https://raw.githubusercontent.com/mdieben/suricata_emerging_updater/master/emergingupdater.py -O /etc/suricata/emergingupdater.py

## Usage
run `python /etc/suricata/emergingthreatsupdater.py`

### Scheduled task
```
crontab -e
0 2 * * * python /etc/suricata/emergingthreatsupdater.py
15 2 * * * suricatasc -c reload-rules
```


## Modifications
Change the version of the rules by changing `rulesurl` to another one found on the [Emerging Threats Server](http://rules.emergingthreats.net/)